## HEAVY METHYL SILAC incorporation
## First decide a directory to work in. Alternatively set it fixed with e.g. setwd()
## Select the combined/txt directory of the MaxQuant run you want to analyze
setwd(choose.dir())

## Read in "peptides.txt" from the directory just pointed to. Parameters are customized for the MaxQuant format.
## peptides.txt can be modified to any other txt file, like proteinGroups.txt. Remember R understands G and g different.
peptides <- read.table("peptides.txt", quote = "\"", header = TRUE, 
                       sep = "\t", stringsAsFactors = FALSE, comment.char = "")
#str(peptides)

## filter Data: exclude contaminants, reverse hits 
filteredpeptides <- peptides[peptides$Reverse != "+",]									## exclude every line with a "+" in the Reverse column
filteredpeptides <- filteredpeptides[filteredpeptides$Potential.contaminant != "+",]	## exclude every line with a "+" in the Contaminant column

filteredpeptides <- filteredpeptides[is.na(filteredpeptides$Ratio.H.L) != TRUE,]		## exclude all peptides that do not have a ratio

methioninepeptides <- filteredpeptides[filteredpeptides$M.Count > 0,]					## isolate all peptides with one or more M

## Find densities
den <- density(1-1/(methioninepeptides$Ratio.H.L+1))

## Find x value where y is max, for later use as incorporation number
inc <- den$x[which.max(den$y)]

## Labels
pep <- paste("Met peptides: ", round(inc,3))

## plot density estimation for all peptides 
plot(												## plotting command
  den, 												## "den" was defined as a kernel density estimation of the incorporation rate
  col="black", 										## colour of the line.
  xlab=expression("Incorporation rate"),			## x axis annotation 				
  ylab=expression("Density"), 	            		## y axis annotation
  ylim=c(0,max(c(den$y))),							## y axis range
  main=paste("Incorporation rate hmSILAC\nN = ",
             length(methioninepeptides$Ratio.H.L)) ## title
)

abline(v=round(inc,3), col='red', lty=2)

legend(												## adds a legend to the plot
  x="topleft", 										## defines position (also possible: "topright", "bottomleft")
  pep, 								  				## names used in the legend		
  col="black", 										## colours for the legend, same order as names
  lwd=1												## linewidth 
)
	