#!/usr/bin/env python
# coding: utf-8

# # hmSEEKER
# 
# Version 2.0
# 
# Developed by Enrico Massignani, PhD
# 
# July 28th, 2020

# In[1]:


import pandas as pd
import numpy as np
import re, math, sklearn, os, itertools, pickle, gzip
from datetime import datetime
from IPython.display import clear_output
# Note: model built with scikit-learn v0.23.1
print(sklearn.__version__)


# In[2]:


#### Define 'modification' class & other functions

from MOD_lib import MOD, PTMs_remapping
from ReadFASTA_lib import GetFasta

def get_nr_data(df,col):
    COL = col.upper()
    df[COL] = np.nan
    df.loc[df['Score_H']>=df['Score_L'],COL] = df.loc[df['Score_H']>=df['Score_L'],col+'_H']
    df.loc[df['Score_H']<=df['Score_L'],COL] = df.loc[df['Score_H']<=df['Score_L'],col+'_L']

def map_peptide(row, seqs):
    return 0
    try:
        cleaned_seq = seqs[row['Proteins'].split(';')[0]]['Seq'].replace("I","X").replace("L","X")
        cleaned_pep = row['Sequence'].replace("I","X").replace("L","X")
        match = re.search(cleaned_pep, cleaned_seq)
        if match:
            return match.start()
        else:
            return -1
    except:
        return -1

def get_label(row):
    # checks that there is no mix of H and L mods
    H_count = 0
    L_count = 0
    for i in row['MODS']:
        if i.state=='L':
            L_count += i.count
        elif i.state=='H':
            H_count += i.count
        else:
            pass
    if (H_count>0) and (L_count==0): #filter mixed labelling
        return 'Heavy'
    elif (H_count==0) and (L_count>0):
        return 'Light'
    else:
        return np.nan
    
def get_mass_shift(row):
    delta_mass = 0
    for i in row['MODS']:
        delta_mass += i.count*i.shift
    return delta_mass

def detect_Met0_and_Met4(row):
    if row['Raw file'].endswith('heavy'):
        return row['Modified sequence'].replace('M\({0}','M(me)') #.replace('M\(..\)\(','M(')
    elif row['Raw file'].endswith('light'):
        return row['Modified sequence'].replace('M\({0}','M(--)') #.replace('M\(..\)\(','M(')


# In[3]:


#### Load & prepare allPeptides.txt
def load_all_peptides(dataset):
    datatypes = {'Raw file': str,
                 'Charge': int,
                 'm/z': float,
                 'Retention time': float,
                 'Modified sequence': str,
                 'Sequence': str,
                 'Intensity': float,
                 'MSMS Scan Numbers': str}
    df = pd.read_csv(os.path.join(dataset,'allPeptides.txt'), sep='\t', dtype=datatypes, 
                     low_memory=False, usecols=list(datatypes.keys()))
    df = df[df.Charge > 1]
    df["Dataset"]= os.path.split(dataset)[-1]
    df['Data_Raw'] = df['Dataset']+'%'+df['Raw file']
    return df


# In[4]:


#### Create Peak Index

def build_peak_index(allpeps):
    tmp = allpeps[allpeps["MSMS Scan Numbers"].isnull()]
    tmp["MSMS Scan Numbers"] = 1
    tmp["MSMS Scan Numbers"] = tmp["MSMS Scan Numbers"].cumsum()
    tmp["MSMS Scan Numbers"] = "Dummy_"+tmp["MSMS Scan Numbers"].apply(str)
    allpeps = pd.concat([tmp,allpeps[allpeps["MSMS Scan Numbers"].notnull()]],ignore_index=True)
    tmp = allpeps[allpeps["MSMS Scan Numbers"].str.contains(";")]
    tmp_exp = tmp["MSMS Scan Numbers"].str.split(";",expand=True)
    exp_list=[]
    for column in tmp_exp.columns:
        tmp_expanded=tmp.loc[tmp_exp[tmp_exp[column].notnull()].index,:]
        tmp_expanded["MSMS Scan Numbers"] = tmp_exp[column][tmp_exp[column].notnull()]
        exp_list.append(tmp_expanded)
    expanded = pd.concat(exp_list)
    all_data = pd.concat([allpeps[~allpeps["MSMS Scan Numbers"].str.contains(";")],expanded])   
    all_data.sort_values('Retention time', inplace=True, kind='mergesort')
    all_data.reset_index(inplace=True)
    return all_data


# In[5]:


#### Load & prepare msms.txt

def load_msms(dataset, min_Score, min_DScore):
    converters={'Proteins': str, 'Scan number': str}
    df = pd.read_csv(os.path.join(dataset,'msms.txt'), sep='\t', dtype=converters, low_memory=False) # provide dtypes
    df = df[df['Proteins'].notnull()]
    df = df[(df['Charge']>1) & (df['Reverse']!='+') & (df['Score']>min_Score) & (df['Delta score']>min_DScore)]
    df = df[~df['Proteins'].str.contains('CON_')]
    df["Dataset"]= os.path.split(dataset)[-1]
    df['Data_Raw'] = df['Dataset']+'%'+df['Raw file']
    return df


# In[6]:


##### loop on methylpeptides

def loop_on_methylpeptides(row, mass_error_cutoff, delta_rt_cutoff, ratio_cutoff, min_LcPrb, m, peak_index): 
    list_of_doublets_lists = []
    temp_peak_index = peak_index[(peak_index.Data_Raw==row['Data_Raw'])].reset_index(drop=True)
    try:
        peak = temp_peak_index[temp_peak_index['MSMS Scan Numbers']==row['Scan number'] ]
        idx = peak.index[0]
        x = max(idx-999,0)
        y = min(idx+999,temp_peak_index.shape[0])
    except:
        return list_of_doublets_lists
    list_of_doublets_lists = list(temp_peak_index.iloc[x:y].apply(lambda x: find_counterpart(x, peak.iloc[0], row,
                                                                                             mass_error_cutoff, delta_rt_cutoff,
                                                                                             ratio_cutoff, min_LcPrb), axis=1))
    list_of_doublets_lists = list(itertools.chain.from_iterable(list_of_doublets_lists))
    return list_of_doublets_lists


# In[7]:


##### Find counterparts

def find_counterpart(peak2, peak1, row, mass_error_cutoff, delta_rt_cutoff, ratio_cutoff, min_LcPrb):
    # PEAK1 corresponds to the peptide in ROW
    doublets_list = []
    state = row['Label']
    delta_mass = row['Mass_shift']
    ptm_attributes = row['MODS']
    if (state=='Light') and (peak2['m/z']>row['m/z']):
        heavy, light = peak2, peak1
    elif (state=='Heavy') and (peak2['m/z']<row['m/z']):
        light, heavy = peak2, peak1
    else:
        return doublets_list
    if (light['Data_Raw']!=heavy['Data_Raw']) or (heavy['Charge']!=light['Charge']) :
        return doublets_list
    check = np.nan
    if (light['Sequence']!=' ') and (heavy['Sequence']!=' ') and (light['Sequence']!=heavy['Sequence']):
        return doublets_list
    mass_error = (((light['m/z']+abs(delta_mass/light['Charge']))/heavy['m/z'])-1)*1000000.0
    delta_rt = light['Retention time']-heavy['Retention time']
    ratio = math.log(heavy['Intensity']/light['Intensity'],2)
    if (abs(mass_error) > mass_error_cutoff) or (abs(delta_rt) > delta_rt_cutoff) or (abs(ratio) > ratio_cutoff):
        return doublets_list
    ## ADD the DOUBLET to DATA FRAME
    for i in ptm_attributes:
        for pos,p in i.lcprb_map.items():
            if (p >= min_LcPrb) and (row['Sequence'][pos] != 'M'): ##  
                doublets_list.append([row['Data_Raw'],
                                      row['Dataset'],
                                      row['Raw file'],
                                      light['MSMS Scan Numbers'],
                                      heavy['MSMS Scan Numbers'],
                                      re.sub(r'M\(me\)|M\(--\)', 'M', row['Modified sequence'].strip('_')),
                                      # row['Modified sequence'].strip('_'),
                                      row['Proteins'].split(';')[0],
                                      row['Sequence'][pos],
                                      #pos+pep_pos+1,    # initiator Met as position +1
                                      pos,
                                      i.symbol, 
                                      p, 
                                      mass_error, 
                                      delta_rt,
                                      round(row['m/z'],4),
                                      delta_mass/row['Charge'],
                                      round(row['Retention time'],2),
                                      row['Gene Names'],
                                      row['Charge'],
                                      round(row['Score'], 0), 
                                      row['Sequence'], 
                                      light['Intensity'], 
                                      heavy['Intensity'],
                                      ratio,
                                      state,
                                      np.nan,np.nan])
    return doublets_list


# In[8]:


## For Shiny
def FolderLineSplit(strings):
    return [_.split(';') for _ in strings]


# In[9]:


def hmSEEKER(INPUT, db, min_Score, min_DScore, min_LcPrb, 
             mass_error_cutoff, delta_rt_cutoff, ratio_cutoff, model_file):
    
    start = datetime.now()
    outdir = 'OUT-'+start.strftime("%Y-%m-%d-%H%M")
    try:
        os.mkdir(outdir)
    except:
        pass
    combined_file = os.path.join(outdir,'combined_hmSILAC_doublets.csv')
    HL_merge_file = os.path.join(outdir,'combined_hmSILAC_doublets_HxL.csv')
    summary_file  = os.path.join(outdir,'combined_hmSILAC_doublets_HxL_summary.csv')
    params_file   = os.path.join(outdir,'run_params.csv')
    seqs = {}
    GetFasta(db, seqs)
    m = MOD.list_all_modifications()
    doublets_columns = ['Data_Raw','Dataset','Rawfile','L-Scan','H-Scan','Peptide',
                        'Lead Protein','Res','Pos','Mod','Loc','ME','dRT',
                        'mz','delta mz','RetTime','Gene','Charge','Score',
                        'AA Seq','Heavy Intensity','Light Intensity','H/L LogRatio','Label',
                        'Probability_TRUE','Prediction']
    frames = []
    for idx,data_line in enumerate(INPUT):
        indir,cell_line = data_line
        print(f'Processing...\t{indir}\t{cell_line}\t({idx+1}/{len(INPUT)})')
        allpeps = load_all_peptides(indir)
        peak_index = build_peak_index(allpeps)
        msms = load_msms(indir, min_Score, min_DScore)
        ## preprocessing Met0 and Met4
        msms['Modified Sequence'] = msms.apply(lambda x: detect_Met0_and_Met4(x), axis=1)
        msms['Met4 Probabilities'] = msms['Modified sequence'].str.strip('_').str.replace('M\(me\)','M(1)').str.replace('\(..\)','')
        msms['Met4'] = msms['Met4 Probabilities'].str.count('(1)')
        msms['Met0 Probabilities'] = msms['Modified sequence'].str.strip('_').str.replace('M\(--\)','M(1)').str.replace('\(..\)','')
        msms['Met0'] = msms['Met0 Probabilities'].str.count('(1)') 
        ## filters
        msms['n_mods'] = msms['Modified sequence'].str.count('\(')
        msms['n_mod_meths'] = msms['Modified sequence'].str.count('M\(')
        msms = msms[msms['n_mod_meths']!=msms['n_mods']]
        msms['MODS'] = msms.apply(lambda x: MOD.get_ptm_attributes(x, m),axis=1)
        msms['All_Localized'] = msms.MODS.apply(lambda x: MOD.all_localized(x,min_LcPrb))
        msms = msms[msms.All_Localized]
        msms['Label'] = msms.apply(lambda x: get_label(x),axis=1)
        msms = msms[~msms.Label.isnull()]
        msms['Mass_shift'] = msms.apply(lambda x: get_mass_shift(x),axis=1)
        # search
        list_of_doublets_lists = list(msms.apply(lambda x: loop_on_methylpeptides(x, mass_error_cutoff, 
                                                                                  delta_rt_cutoff, ratio_cutoff,
                                                                                  min_LcPrb, m, peak_index), axis=1))
        list_of_doublets = list(itertools.chain.from_iterable(list_of_doublets_lists))
        partial_df = pd.DataFrame(list_of_doublets, columns=doublets_columns)
        partial_df['Cell_line'] = cell_line
        # print partial results
        partial_df.to_csv(os.path.join(indir,'Partial_hmSILAC_doublets.csv'),index=False)
        frames.append(partial_df)
        clear_output(wait=True)
        
    ## COMBINE ALL DATASETS
    print('combining all datasets...')
    doublets_df = pd.concat(frames, ignore_index=True)

    ## MACHINE LEARNING
    with gzip.open(model_file,'rb') as infile:
        MODEL = pickle.load(infile)
    try:  ## comment these 2 lines if you are not using machine learning
        doublets_df.Probability_TRUE = MODEL.predict_proba(abs(doublets_df[['H/L LogRatio','ME','dRT']]))[:,1]
        doublets_df.Prediction = MODEL.predict(abs(doublets_df[['H/L LogRatio','ME','dRT']]))
        print('Doublets predicted with Machine Learning')
    except:
        print(f"Could not apply classifier: '{model_file}'\n"+
              "Check your sklearn version\n"+
              "Version 0.23.1 required to run the ML model\n"+
              f"Your version is {sklearn.__version__}")
    
    ## Print COMBO
    doublets_df.to_csv(combined_file, index=False) # raw combined output
    
    ## Separate the cell lines
    cell_line_dfs = [] 
    for l in set(doublets_df.Cell_line):
        tmp = doublets_df[doublets_df.Cell_line==l]
        tmp = tmp[['Peptide']].drop_duplicates()
        tmp[f'Cell_line_{l}'] = 'ID'
        cell_line_dfs.append(tmp.rename(columns={'Peptide':'PEPTIDE'}))
    doublets_df.drop(columns=['Cell_line'], inplace=True)
    
    ## MERGE H & L DOUBLETS
    print('Matching L and H peptides...')
    regexp = re.compile("_light$|_heavy$")
    doublets_df.Rawfile = doublets_df.Rawfile.str.replace(regexp, "")
    heavy_doublets = doublets_df.loc[doublets_df.Label=='Heavy']
    x = (list(filter(re.compile(".*Light.*").match, heavy_doublets.columns)))
    heavy_only = heavy_doublets.drop(columns=x)
    light_doublets = doublets_df.loc[doublets_df.Label=='Light']
    x = (list(filter(re.compile(".*Heavy.*").match, heavy_doublets.columns)))
    light_only = light_doublets.drop(columns=x)

    merged_doublets = light_only.merge(heavy_only, how='outer', on=['Rawfile','H-Scan','L-Scan'], suffixes=('_L', '_H'))
    to_b_removed = merged_doublets.loc[(merged_doublets.Pos_H!=merged_doublets.Pos_L)&
                                       (merged_doublets.Pos_H==merged_doublets.Pos_H)&
                                       (merged_doublets.Pos_L==merged_doublets.Pos_L)].index
    BLANK = '_'
    merged_doublets[['Score_H','Score_L']] = merged_doublets[['Score_H','Score_L']].fillna(0)
    merged_doublets[['Peptide_H','Peptide_L',
                     'AA Seq_H','AA Seq_L']] = merged_doublets[['Peptide_H','Peptide_L',
                                                                'AA Seq_H','AA Seq_L']].fillna(BLANK)
    merged_doublets['CLASS'] = np.nan
    ## order of operations matters!
    merged_doublets.loc[merged_doublets['AA Seq_L']!=merged_doublets['AA Seq_H'],'CLASS'] = 'Putative FP'
    merged_doublets.loc[merged_doublets['AA Seq_L']==merged_doublets['AA Seq_H'], 'CLASS'] = 'Mismatched'
    merged_doublets.loc[merged_doublets['Peptide_L']==merged_doublets['Peptide_H'], 'CLASS'] = 'Matched'
    merged_doublets.loc[(merged_doublets['Peptide_L']==BLANK)|(merged_doublets['Peptide_H']==BLANK),'CLASS'] = 'Rescued'
    merged_doublets.drop(index=to_b_removed, inplace=True)
    merged_doublets.to_csv(HL_merge_file, index=False)
    # get nr output
    print('Printing non-redundant final output...')
    essential_columns = ['Peptide','Res','Pos','Mod', 'Lead Protein','Gene', 'mz', 'RetTime', 'Charge',
                         'Probability_TRUE','Prediction','H/L LogRatio','ME','dRT','Score','Loc']
    for i in essential_columns:
        get_nr_data(merged_doublets,i)

    ## NON-REDUNDANT LIST
    essential_columns = ['Rawfile','H-Scan','L-Scan','CLASS','PEPTIDE','MZ','CHARGE','RETTIME','SCORE',
                         'RES','POS','MOD','LOC', 'LEAD PROTEIN','GENE',
                         'PROBABILITY_TRUE','PREDICTION','H/L LOGRATIO','ME','DRT']
    doublets_summary = merged_doublets[essential_columns]
    doublets_summary.sort_values('SCORE', inplace=True, kind='mergesort', ascending=False)
    doublets_summary.sort_values('CLASS', inplace=True, kind='mergesort')
    doublets_summary.sort_values('PROBABILITY_TRUE', inplace=True, kind='mergesort', ascending=False)
    doublets_summary.drop_duplicates(['PEPTIDE','RES','POS','MOD'], inplace=True)
    for df in cell_line_dfs:
        doublets_summary = df.merge(doublets_summary, on='PEPTIDE', how='right')
    ##
    amb_peps_df = PTMs_remapping.ambiguous_peptides(doublets_summary[['PEPTIDE']].drop_duplicates(), 
                                                    seqs)
    doublets_summary = doublets_summary.merge(amb_peps_df.rename(columns={'Peptide':'PEPTIDE'}),
                                              on='PEPTIDE', how='outer')
    doublets_summary.POS = doublets_summary.Peptide_position + doublets_summary.POS + 1 ## Initiator Met in posiion 1  
    ##
    doublets_summary.to_csv(summary_file, index=False)
    
    ## PRINT PARAMS
    parameters_dict = {'Input folders': INPUT,
                       'Fasta Database': db,
                       'LocProbability threshold': min_LcPrb,
                       'AndromedaScore threshold': min_Score,
                       'DeltaScore threshold': min_DScore,
                       'ME cutoff': mass_error_cutoff,
                       'dRT cutoff': delta_rt_cutoff,
                       'logRatio cutoff': ratio_cutoff,
                       'Model file': model_file }
    pd.DataFrame(parameters_dict).to_csv(params_file)
    
    clear_output(wait=True)
    print(f'Runtime = {datetime.now() - start}')
    return f'Runtime = {datetime.now() - start}'