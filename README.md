# hmSEEKER 

## What is this repository for? 

**hmSEEKER** helps researchers that are interested in protein methylation to analyze heavy methyl SILAC experiments.
This version is written in **Python** as a **Jupyter notebook**. Very basic knowledge of programming and command line interfaces are enough to get it running.
The Python code can also be run through a **Shiny application** via the R package **Reticulate**.

## Setup

We recommend running hmSEEKER within a conda environment; this will ensure reproducibility and also help in managing the installation of Python and the needed packages.
`conda create -n hmseeker-env python=3.8 scikit-learn=0.23.1 jupyterlab pandas`

If git is available on your machine, clone this repository on you machine with:
`git clone https://EMassi@bitbucket.org/2020-methylproteome-project/hmseeker.git`
otherwise click on the Downloads tab on the left of this webpage and download this repository as a zipped folder.

This repository includes a sample dataset that can be used for testing.

If you wish to run hmSEEKER as a Shiny app, install the following packages in your R environment:
```
install.packages(shiny)
install.packages(shinyFiles)
install.packages(reticulate)
```

## How do I run hmSEEKER?

**Update!** The latest version of MaxQuant no longer use abbreviations for ptms names, instead they insert the full modification name in the peptide modified sequence. This change broke hmSEEKER's code, as the script expects PTMs to be represented by a 2-character string in parentheses.
If the msms.txt file can be opened with Microsoft Excel (or similar), this can be easily fixed with the "find and replace" tool (i.e. replace "R(Methyl (KR))" with "R(me)", and so on). (Only replace in the "Modified Sequence" column!!)

#### ...with Jupyter
1. Create a folder and store the `allPeptides.txt` and `msms.txt` files from MaxQuant output into the folder
1. From your command line, activate the hmseeker environment (`conda activate hmseeker-env`) and run Jupyter (`jupyter lab` or `jupyter notebook`)
	* Alternatively, use the Anaconda navigator
1. Navigate to the "hmSEEKER" folder and open `hmSEEKER-notebook.ipynb`
1. In the "Input Parameters" section of the notebook specify the following parameters:
	* `db` = path to the reference FASTA file (it should be a `.gz` zipped file)
	* `INPUT` = list of `[dataset,cell-line]` pairs, where `dataset` is the path to the folder you create in step 1 (containing `allPeptides.txt` and `msms.txt`) and `cell-line` is the name of the analyzed cell line.
	* `min_Score`, `min_DScore` and `min_LcPrb` = Peptide quality filtering criteria, i.e. the minimum **Score** and **Delta Score** of the peptides and the minimum **Localization Probability** of the detected PTMs.
	* `mass_error_cutoff`, `delta_rt_cutoff` and `ratio_cutoff` = Doublet search critaria; only doublets that respect these thresholds for **mass error, retention time difference and ratio** will be reported.
	* `model_file` = path to the machine learning model file (it should be a `.gz` zipped file); doublets detected by the initial search will be evaluated by the model to distinguish real hmSILAC peptides from false positives.
1. Run the code inside each cell; hmSEEKER will produce a `.csv` output file for each dataset that was analysed, plus a “combined” file. 
1. The final output can be found in the file named `[date]-[time]-combined_hmSILAC_doublets_HxL_summary.csv`.

#### ...with R Shiny
1. Create a folder and store the `allPeptides.txt` and `msms.txt` files from MaxQuant output into the folder
1. Open `hmSEEKER-GUI.R` with Rstudio (regular R also works).
1. Look for the following line of code at the top of the script: `use_condaenv(condaenv = "base", conda = "auto", required = FALSE)`; replace `"base"` with the name of your conda environment (i.e. `"hmseeker-env"`).
1. Click on the `Run app` button; this will open an interactive web page where you can input all the parameters.
	* Use the panel on the left to insert the parameters; the right panel will automatically update.
1. Select the dataset(s) you want to analyze:
	* Click on the (Select folder) button and navigate to the folder you create in step 1 (containing `allPeptides.txt` and `msms.txt`).
	* Specify the name of the analyzed cell line.
	* Click on (Add folder) to add the selected folder to the queue of datasets to be analyzed by hmSEEKER.
	* Repeat these steps as many times as you wish to analyze multiple experiments in one run.
1. Click on (Select FASTA) and choose the reference FASTA file (it should be a `.gz` zipped file).
1. Click on (Select ML model file) and choose the machine learning model file (it should be a `.gz` zipped file).
1. Click on the (Start analysis) button at the bottom of the panel to start the analysis.

## Contacts and References

* My current e-mail adress: enrico.massignani@ieo.it
* My current boss's e-mail address: tiziana.bonaldi@ieo.it
* [hmSEEKER manuscript](https://doi.org/10.1002/pmic.201800300)