#!/usr/bin/env python
# coding: utf-8

import re, gzip, numpy

def GetFasta(infile, outseqs):
    with gzip.open(infile, 'rt') as FASTA:
        for l in FASTA:
            if l[0] == ">":
                gene = numpy.nan
                try:
                    uniac, entry, gene = re.search('\|(.+)\|(\w+).*GN=(\w+)',l).groups()
                except:
                    uniac, entry = re.search('\|(.+)\|(\w+)',l).groups()
                outseqs[uniac] = {}
                outseqs[uniac]['Seq'] = ''
                outseqs[uniac]['Entry'] = entry
                outseqs[uniac]['Gene'] = gene
            elif l[0] != ">":
                outseqs[uniac]['Seq'] += (l.strip('\n'))